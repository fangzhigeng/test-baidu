import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart  # 混合MIME格式，支持上传附件
from email.header import Header  # 用于使用中文邮件主题
import sys
sys.path.append('..')  # 提升一级到项目更目录下
from config.config import *  # 从项目根目录下导入

def send_email(report_file):
    msg = MIMEMultipart()  # 混合MIME格式
    msg.attach(MIMEText(open(report_file, encoding='utf-8').read(),'html', 'utf-8'))  # 添加html格式邮件正文（会丢失css格式）

    # 2. 组装Email头（发件人，收件人，主题）
    msg['From'] = 'ying839415@sina.com'  # 发件人
    msg['To'] = 'fang839415@sina.com'  # 收件人
    msg['Subject'] = Header(subject, 'utf-8')    # 从配置文件中读取

    # 3. 构造附件1，传送当前目录下的 test.txt 文件
    att1 = MIMEText(open(report_file, 'rb').read(), 'base64', 'utf-8')  # 二进制格式打开
    att1["Content-Type"] = 'application/octet-stream'
    att1["Content-Disposition"] = 'attachment; filename="{}"' .format(report_file) # filename为邮件中附件显示的名字
    msg.attach(att1)

    try:
    # 3. 连接smtp服务器并发送邮件
        smtp = smtplib.SMTP_SSL(smtp_server)  # smtp服务器地址 使用SSL模式
        smtp.login(smtp_user, smtp_password)
        smtp.sendmail(sender, receiver, msg.as_string())
        logging.info("邮件发送完成！")
    except Exception as e:
        logging.error(str(e))
    finally:
        smtp.quit()