import xlrd

def excel_to_list(data_file,sheet):
    data_list = []  # 新建个空列表，来乘装所有的数据
    wb = xlrd.open_workbook(data_file)
    sh = wb.sheet_by_name(sheet)  # 按工作簿名定位工作表
    header=sh.row_values(0)# 获取标题行数据
    for i in range(1,sh.nrows):   # 跳过标题行，从第二行开始取数据
        d=dict(zip(header,sh.row_values(i)))
        data_list.append(d)
    return data_list   # 列表嵌套字典格式，每个元素是一个字典

def get_test_data(data_list,case_name):
    for case_data in data_list:
        if case_name == case_data['case_name']:
            return case_data
        # 如果查询不到会返回None

if __name__=="__main__":
   data_list=excel_to_list("../../data/test_user_data.xlsx", "TestUserLogin")
   case_data=get_test_data(data_list,'test_user_login_normal')
   print(case_data)
