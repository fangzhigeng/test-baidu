import unittest
import requests
import json  # 用来转化excel中的json字符串为字典
import os
import sys
sys.path.append("../..")
from config.config import *
from lib.read_excel import *  # 从项目路径下导入
from lib.case_log import log_case_info  # 从项目路径下导入


class TestUserLogin(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_list = excel_to_list(os.path.join(data_path,"test_user_data.xlsx"), "TestUserLogin")
        # cls.data_list 同 self.data_list 都是该类的公共属性

    def test_user_login_normal(self):
        case_data = get_test_data(self.data_list, 'test_user_login_normal')
        if not case_data:  # 有可能为None
            print("用例数据不存在")
        url = case_data.get('url')  # 从字典中取数据，excel中的标题也必须是小写url
        data= case_data.get('data')  # 注意字符串格式，需要用json.loads()转化为字典格式
       # headers1 = case_data.get('headers')
        headers = {'Content-Type': 'application/json;charset=UTF-8'}
        expect_res = case_data.get('expect_res')  # 期望数据
        res = requests.post(url, data=data, headers=headers)  # 表单请求，数据转为字典格式
        logging.info("测试用例：{}".format('test_user_login_normal'))
        logging.info("url:{}".format(url))
        logging.info("请求参数：{}".format(data))
        logging.info("期望结果：{}".format(expect_res))
        logging.info("实际结果：{}".format(res.text))
        self.assertIn(expect_res,res.text)
        print(1)

if __name__ == '__main__':     # 非必要，用于测试我们的代码
    unittest.main(verbosity=2);