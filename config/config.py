import logging
import os

# 项目路径
prj_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 当前文件的绝对路径的上一级，__file__指当前文件

data_path = os.path.join(prj_path,'data')
test_path = os.path.join(prj_path,'test')

log_file = os.path.join(prj_path, 'log','log.txt')  # 也可以每天生成新的日志文件
report_file = os.path.join(prj_path, 'report','report.html')  # 也可以每次生成新的报告

# log配置
logging.basicConfig(level=logging.DEBUG,  # log level
                    format='[%(asctime)s] %(levelname)s [%(funcName)s: %(filename)s, %(lineno)d] %(message)s',  # log格式
                    datefmt='%Y-%m-%d %H:%M:%S',  # 日期格式
                    filename=log_file,  # 日志输出文件
                    filemode='a')  # 追加模式


# 数据库配置
db_host = '127.0.0.1'   # 自己的服务器地址
db_port = 3306
db_user = 'root'
db_passwd = '123456'
db = 'python练习'

# 邮件配置
smtp_server = 'smtp.sina.com'
smtp_user = 'ying839415@sina.com'
smtp_password = 'eaa698a738014361'

sender = smtp_user  # 发件人
receiver = 'fang839415@sina.com'  # 收件人
subject = '接口测试报告'  # 邮件主题